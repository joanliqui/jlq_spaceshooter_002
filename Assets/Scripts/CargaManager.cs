﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CargaManager : MonoBehaviour
{
    private float currentTime = 0;
    [SerializeField] float timeToPass;
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;

        if(currentTime >= timeToPass){
            SceneManager.LoadScene("SampleScene");
        }
    }
}
