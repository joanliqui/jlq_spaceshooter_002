﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtilleroBullet : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;

    public float artDamage;

    void Awake(){
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate(){
        rb.velocity = new Vector3(-speed * Time.deltaTime, 0f, 0f);
    }

    void OnTriggerEnter(Collider other){
        if(other.tag =="Player"){
            Destroy(gameObject);
        }
        else if (other.CompareTag("Walls")){
            Destroy(gameObject);
        }
    }
}
