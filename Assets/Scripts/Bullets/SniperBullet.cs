﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperBullet : MonoBehaviour
{
    public float speed;  
    public float sniperDamage;
    private GameObject player;

    void Awake(){
        player = GameObject.FindGameObjectWithTag("Player");
        transform.LookAt(player.transform);
        }
void Update(){
    transform.Translate (0f, 0f, speed * Time.deltaTime);

}

    void OnTriggerEnter(Collider other){
        if(other.tag == "Player"){
            Destroy(gameObject);
        }
        else if(other.tag == "Walls"){
            Destroy(gameObject);
        }
    }
}
