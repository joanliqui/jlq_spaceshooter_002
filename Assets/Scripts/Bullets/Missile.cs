﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    private GameObject enemyTarget;
    private GameObject kamikaze;
    private GameObject sniper;

    [SerializeField] float speed;
    public float missileDamage;

    private AudioSource sound;
    
    void Start(){
        enemyTarget = GameObject.FindGameObjectWithTag("Enemy");
        kamikaze = GameObject.FindGameObjectWithTag("Kamikaze");
        sniper = GameObject.FindGameObjectWithTag("Sniper");
        sound = GetComponent<AudioSource>();
    }
    void Update () {

        //Puertas Logicas
        if(enemyTarget == null && kamikaze == null && sniper == null){
            transform.Translate( 0, 0, speed * Time.deltaTime);
        }
        else if(enemyTarget == true && kamikaze == null && sniper == null){
        transform.LookAt (enemyTarget.transform.position);
        transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
        //transform.position = Vector2.MoveTowards(transform.position,enemyTarget.position, speed *Time.deltaTime);
        }
        else if(enemyTarget == null && kamikaze == true && sniper == null){
            transform.LookAt (kamikaze.transform.position);
            transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
        }
        else if(enemyTarget == null && kamikaze == null && sniper == true){
            transform.LookAt (sniper.transform.position);
            transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
            }
        else if(enemyTarget == true && kamikaze == true && sniper == null){
            transform.LookAt (kamikaze.transform.position);
            transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
        }
        else if(enemyTarget == null && kamikaze == true && sniper == true){
            transform.LookAt (kamikaze.transform.position);
            transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
        }
        else if(enemyTarget == true && kamikaze == null && sniper == true){
            transform.LookAt (enemyTarget.transform.position);
            transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
        }
        else if(enemyTarget == true && kamikaze == true && sniper == true){
            transform.LookAt (kamikaze.transform.position);
            transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
        }
    }

    public void OnTriggerEnter(Collider other){
        if(other.tag == "Enemy" || other.tag == "Kamikaze" || other.tag == "Sniper"){
            sound.Play();
            Destroy(gameObject);
        }
        else if(other.tag =="Walls"){
            Destroy(gameObject);
        }
    }
}
