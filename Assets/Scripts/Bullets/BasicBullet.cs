﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBullet : MonoBehaviour
{
    public float speed;
    public float bulletDamage;
    private AudioSource sound;
    void Awake(){
        sound = GetComponent<AudioSource>();
    }
    void Update(){
        transform.Translate (speed * Time.deltaTime, 0, 0);
    }

    public void OnTriggerEnter(Collider other){
        if(other.tag == "Walls"){
            Destroy(gameObject);
        }
        else if(other.tag == "Enemy" || other.tag =="Kamikaze" || other.tag == "Sniper"){
            sound.Play();
            Destroy(gameObject);
        }
    }
}
