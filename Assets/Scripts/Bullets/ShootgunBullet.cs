﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootgunBullet : MonoBehaviour
{
    public float speed = 5;
    public float shDamage;
    private AudioSource sound;
    void Awake(){
        sound = GetComponent<AudioSource>();
    }

    void Update(){
        transform.Translate (speed * Time.deltaTime, 0, 0);
    }

    void OnTriggerEnter(Collider other){
        if( other.tag == "Walls"){
            Destroy(gameObject);
        }
        else if(other.tag == "Enemy" || other.tag == "Kamikaze" || other.tag == "Sniper"){
            sound.Play();
            Destroy(gameObject);
        }
    }
}
