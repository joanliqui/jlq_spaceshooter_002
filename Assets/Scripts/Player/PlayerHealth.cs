﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public float maxHealth = 100f;
    public float currentHealth;

    public bool iAmDead;
    public bool iAmDamaged;

    [SerializeField] float outDamage = 1;
    [SerializeField] Vector2 limits;
    private Material mat;
    [SerializeField] MeshCollider col;

    //SOUND

    [SerializeField] AudioSource[] sounds;
    void Awake(){
        currentHealth = maxHealth;
        mat = GetComponent<Renderer>().material;
    }
    void Update(){
             //Limites
        if(transform.position.x > limits.x){
            currentHealth -= outDamage;
            print(currentHealth);
        }
        else if(transform.position.x < -limits.x){
             currentHealth -= outDamage;
             print(currentHealth);
        }
         if (transform.position.z > limits.y){
            currentHealth -= outDamage;
            print(currentHealth);
        }
        else if(transform.position.z < -limits.y){
            currentHealth -= outDamage;
            print(currentHealth);
        }

        //Vida maxima 100
        if(currentHealth >= maxHealth){
            currentHealth = maxHealth;
        }
        //Muerte
        if(currentHealth <= 0){
            currentHealth = 0;
            iAmDead = true;
            Dead();
        }
    }

    private void OnTriggerEnter(Collider other){
        if(other.CompareTag("EnemyBullet")){
            currentHealth -= other.GetComponent<ArtilleroBullet>().artDamage; 
            print(currentHealth);
            StartCoroutine(Damaged());
        }
        else if(other.tag == "Enemy"){
            currentHealth -= other.GetComponent<EnemyArtillero>().explDamage;
            print(currentHealth);
            StartCoroutine(Damaged());
        }
        else if(other.tag == "Kamikaze"){
            currentHealth -= other.GetComponent<EnemyKamikaze>().kamikazeDamage;
            print(currentHealth);
            StartCoroutine(Damaged());
        }
        else if(other.tag == "SniperBullet"){
            currentHealth -= other.GetComponent<SniperBullet>().sniperDamage;
            print(currentHealth);
            StartCoroutine(Damaged());
        }
        else if(other.tag == "EnemySniper"){
            currentHealth -= other.GetComponent<EnemySniper>().explDamage;
            print(currentHealth);
            StartCoroutine(Damaged());
        }
        else if(other.tag == "Botequin"){
            currentHealth += other.GetComponent<Botequin>().curacion;
            print(currentHealth);
            StartCoroutine(Healthy());
        }
    }

    IEnumerator Damaged(){
        col.enabled = false;
        for( int contador = 0; contador <= 3; contador ++){
            mat.color = Color.red;
            sounds[0].Play();
            yield return new WaitForSeconds(0.1f);
            mat.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }
        col.enabled = true;
    }

    IEnumerator Healthy(){
        mat.color = Color.green;
        sounds[1].Play();
        yield return new WaitForSeconds(0.2f);
        mat.color = Color.white;
    }

    public void Dead(){
        if(iAmDead){
            SceneManager.LoadScene("EndingScreen");
        }
    }

}
