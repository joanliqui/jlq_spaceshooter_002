﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotLife : MonoBehaviour
{
    //Movement
    public GameObject player;

    public float speed;
    public float maxDistance = 3;

    //ColorLife
    private Material mat;
    public Color[] colorLife;

    private PlayerHealth playerLife;

    private float greenLife = 100;
    private float yellowLife = 66;
    private float redLife = 33;

    void Awake(){
        mat = GetComponent<Renderer>().material;
        playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        transform.position = new Vector3( -11f, 0f, 2f);
    }

    void Update(){
        //Movimiento
        if(Vector3.Distance(transform.position, player.transform.position) > maxDistance ){
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed);
            
        }
        else if (Vector3.Distance(transform.position, player.transform.position) < maxDistance){
            transform.position = transform.position;
        }

        //ColorVida
        if(playerLife.currentHealth == playerLife.maxHealth){
            mat.color = colorLife[3];
        }
        else if(playerLife.currentHealth < greenLife && playerLife.currentHealth > yellowLife ){
            mat.color = colorLife[0];
        }
        else if(playerLife.currentHealth <= yellowLife && playerLife.currentHealth > redLife){
            mat.color = colorLife[1];
        }
        else if(playerLife.currentHealth <= redLife && playerLife.currentHealth > 0){
            mat.color = colorLife[2];
        }
        else if(playerLife.currentHealth <= 0){
            mat.color = colorLife[3];
        }
            
    }


}
