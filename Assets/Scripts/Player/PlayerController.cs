﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movementSpeed;
    private Rigidbody rb;

    private float moveHorizontal;
    private float moveVertical;
    private Vector3 movement;

    public Weapons[] currentWeapon;

    public int nMissiles;
    public int nShgBullets;

    public bool missileActivated = false;
    public bool shgActivated = false;
    private float shootTime = 0;

    private PlayerHealth pLife;

    //SCORE
    private ScoreManager sm;
    //Sound

    private AudioSource sound;

    void Awake(){
        rb = GetComponent<Rigidbody>();
        sm = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        sound = GetComponent<AudioSource>();
        rb.position = new Vector3( -9f, -2f, 0f);
        pLife = GetComponent<PlayerHealth>();
        missileActivated = false;
        shgActivated = false;
        nMissiles = 0;
        nShgBullets = 0;

    }

    void FixedUpdate () {
        //Movimiento Por Fisicas
        if(pLife.iAmDead){
            return;
        }

        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");
        movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * movementSpeed;
    }

    void Update(){
        if(pLife.iAmDead){
            return;
        }

        shootTime += Time.deltaTime;
        //SHOOTS
        //laser
        if(Input.GetButton("Jump") && shootTime >= currentWeapon[0].Cadencia() && !missileActivated && !shgActivated){
            shootTime = 0f;
            nMissiles = 0;
            nShgBullets = 0;
            currentWeapon[0].Shoot();
        }
        //missiles
        else if(Input.GetButton("Jump") && shootTime >= currentWeapon[1].Cadencia() && missileActivated && !shgActivated){
            shootTime = 0f;
            currentWeapon[1].Shoot();
            nMissiles --;
            print(nMissiles);
            //Vuelta al Laser
            if(nMissiles == 0){
                missileActivated = false;
            }
        }
        //shootgun
        else if(Input.GetButton("Jump") && shootTime >= currentWeapon[2].Cadencia() && !missileActivated && shgActivated){
            shootTime = 0;
            currentWeapon[2].Shoot();
            nShgBullets --;
            Debug.Log(nShgBullets);
            //Vuelta al laser
            if(nShgBullets == 0){
                shgActivated = false;
            }
        }
        sm.MissilesUI(nMissiles);
        sm.ShootgunUI(nShgBullets);
    
    }
    public void OnTriggerEnter(Collider other){
          if(other.tag == "MissilesPowerUp"){
               missileActivated = true;
               nMissiles = 7;
               shgActivated = false;
               nShgBullets = 0;
               sound.Play();
         }
         else if(other.tag =="ShootgunPU"){
             shgActivated = true;
             nShgBullets = 10;
             missileActivated = false;
             nMissiles = 0;
             sound.Play();
         }
     }
}
