﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    private int currentScore;
    [SerializeField] Text scoreText;

    //Bullets
    [SerializeField] GameObject[] uiImages;
    [SerializeField] Text[] nBullets;
    private int currentMissileAmmo;
    private int currentShgAmmo;

    
    void Awake(){
        currentScore = 0;
        scoreText.text = currentScore.ToString("0000");
        currentMissileAmmo = 0;
        currentShgAmmo = 0;
        nBullets[0].text = currentMissileAmmo.ToString("00");
        nBullets[1].text = currentShgAmmo.ToString("00");
    }

    public void AddScore(int value){
        currentScore += value;
        scoreText.text = currentScore.ToString("0000");
        if(currentScore > GetRecord()){
            SaveScore(currentScore);
        }
        SaveActualScore(currentScore);
    }

    public void MissilesUI(int bullets){
        currentMissileAmmo = bullets;
        if(currentMissileAmmo > 0){
            uiImages[0].SetActive(true);
            nBullets[0].color = Color.white;
        }
        else{
            uiImages[0].SetActive(false);
            nBullets[0].color = new Color ( 1f, 1f, 1f, 0f);
        } 
        nBullets[0].text = currentMissileAmmo.ToString("00");

    }
    public void ShootgunUI(int bullets){
        currentShgAmmo = bullets;
        if(currentShgAmmo > 0){
            uiImages[1].SetActive(true);
            nBullets[1].color = Color.white;
        }
        else{
            uiImages[1].SetActive(false);
            nBullets[1].color = new Color( 1f, 1f, 1f, 0f);
        }
        nBullets[1].text = currentShgAmmo.ToString("00");
        
    }

    //PlayerPrefs
    public int GetRecord(){
        return PlayerPrefs.GetInt("MaxScore", 0);
    }

    public void SaveScore(int currentPoints){
        PlayerPrefs.SetInt("MaxScore", currentPoints);
    }

    public int GetScore(){
        return PlayerPrefs.GetInt("Score", 0);
    }
    public void SaveActualScore(int currentScore){
        PlayerPrefs.SetInt("Score", currentScore);
    }
}
