﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private AudioSource music;


    void Awake(){
        music = GetComponent<AudioSource>();
        music.volume = 0.5f;
        music.Play();

    }
}
