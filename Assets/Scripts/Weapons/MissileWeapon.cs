﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileWeapon : Weapons
{
    public GameObject missile;
    public float cadencia;
    private AudioSource sound;

    void Awake(){
        sound = GetComponent<AudioSource>();
    }

    public override float Cadencia(){

        return cadencia;
    }

      public override void Shoot(){
        Instantiate (missile, this.transform.position, Quaternion.Euler(0,90,0), null);
        sound.Play();
    }
}
