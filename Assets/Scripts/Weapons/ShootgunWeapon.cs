﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootgunWeapon : Weapons
{
    public GameObject shootgunBullet;
    public float cadencia = 0.5f;
    private AudioSource sound;

    void Awake(){
        sound = GetComponent<AudioSource>();
    }
    public override float Cadencia(){
        return cadencia;
    }

    public override void Shoot(){
        Instantiate (shootgunBullet, this.transform.position, Quaternion.Euler( 0f, 30f, 0f), null);
        Instantiate (shootgunBullet, this.transform.position, Quaternion.Euler( 0f, 20f, 0f), null);
        Instantiate (shootgunBullet, this.transform.position, Quaternion.Euler( 0f, 10f, 0f), null);
        Instantiate (shootgunBullet, this.transform.position, Quaternion.identity, null);
        Instantiate (shootgunBullet, this.transform.position, Quaternion.Euler( 0f, -10f, 0f), null);
        Instantiate (shootgunBullet, this.transform.position, Quaternion.Euler( 0f, -20f, 0f), null);
        Instantiate (shootgunBullet, this.transform.position, Quaternion.Euler( 0f, -30f, 0f), null);
        sound.Play();


    }
}
