﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapons : MonoBehaviour
{
    public abstract float Cadencia();

    public abstract void Shoot();
}
