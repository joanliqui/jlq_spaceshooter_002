﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicLaser : Weapons
{
    public GameObject bullet;
    public float cadencia;

    public Collider player;
    public bool secondLaser;


    private Vector3 posicion;

    //Sound
    private AudioSource sound;

    void Awake(){
        posicion = new Vector3 (0,0,-2);
        sound = GetComponent<AudioSource>();
    }

    public override float Cadencia(){

        return cadencia;
    }

    public void OnTriggerEnter(Collider other){
        if(other.tag == "secondLaser"){
            print("asa");
            secondLaser = true;
        }
    }
    public override void Shoot(){
        Instantiate (bullet, this.transform.position, Quaternion.identity, null);
        if(secondLaser){
            Instantiate (bullet, this.transform.position + posicion, Quaternion.identity, null);
        }
        sound.volume = 0.5f;
        sound.Play();
    }

}
