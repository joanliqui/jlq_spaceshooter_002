﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class EndingManager : MonoBehaviour
{
    public Text score;
    public Text record;
    private AudioSource sound;

    void Awake(){
        score.text = "SCORE: " + PlayerPrefs.GetInt("Score"); 
        record.text ="RECORD: " + PlayerPrefs.GetInt("MaxScore");
        sound = GetComponent<AudioSource>();
    }

    public void PlayAgain(){
        StartCoroutine(Sonido());
        SceneManager.LoadScene("SampleScene");
    }
    IEnumerator Sonido(){
        sound.Play();
        yield return new WaitForSeconds(0.5f);
    }

    public void GoMenu(){
        SceneManager.LoadScene("TitleScreen");
    }
    
    public void Exit(){
        Application.Quit();
    }
}
