﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Animator[] canvasButtons;
    public Animator animationRobot;
    //public Animator animationButton;
    public Animator bulletThis;
    private AudioSource sound;

    void Awake(){
        sound = GetComponent<AudioSource>();
    }

    public void PressPlay(){
        //animationButton.SetBool("disapears", true);
        canvasButtons[0].SetBool("disapears", true);
        canvasButtons[1].SetBool("disapears", true);
        canvasButtons[2].SetBool("disapears", true);
        canvasButtons[3].SetBool("disapears", true);
        sound.Play();
        bulletThis.SetBool("disapears", true);
        StartCoroutine(Wait());
    }

    IEnumerator Wait(){
        yield return new WaitForSeconds(1.0f);
        animationRobot.SetBool("PlayClick", true);
        yield return new WaitForSeconds (1.05f);
        SceneManager.LoadScene("Carga");
    }

    public void PressExit(){
        Application.Quit();
    }

    public void PressCredits(){
        SceneManager.LoadScene("Credits");
    }
}
