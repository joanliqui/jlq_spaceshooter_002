﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKamikaze : MonoBehaviour
{
    public float speed;
    private GameObject target;

    public float kamikazeDamage;

    public float maxHealth = 35;
    public float currentHealth;

    //PowerUps;
    [SerializeField] GameObject[] powerUp;
    private float randomPU;
    private Vector3 puPosition;

    //SCORE
    private ScoreManager sm;
    [SerializeField] int score;

    void Awake(){
        currentHealth = maxHealth;
        target = GameObject.FindGameObjectWithTag("Player");
        randomPU = Random.Range( 0f, 100f);
        sm = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }
    void Update(){
        transform.LookAt(target.transform);
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);

        //Muerte
        puPosition = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z);
        if(currentHealth <= 0){
            if(randomPU <= 100f && randomPU >50f){
                Destroy(gameObject);
            }
            else if (randomPU <= 50f && randomPU > 38f){
                Instantiate (powerUp[0], puPosition, Quaternion.identity, null);
            }
            else if(randomPU <= 38f && randomPU > 20f){
                Instantiate (powerUp[1], puPosition, Quaternion.identity, null);
            }
            else if(randomPU <=20f && randomPU >= 0){
                Instantiate (powerUp[2], puPosition, Quaternion.identity, null);
            }
            print(randomPU);

            sm.AddScore(score);
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other){
        if(other.tag == "Player"){
            Destroy(gameObject);
        }
        else if(other.tag == "Missile"){
            currentHealth -= other.GetComponent<Missile>().missileDamage;
        }
        else if(other.tag == "PlayerBullet"){
            currentHealth -= other.GetComponent<BasicBullet>().bulletDamage;
        }
        else if(other.CompareTag("ShootgunBullet")){
            currentHealth -= other.GetComponent<ShootgunBullet>().shDamage;
        }
        else if(other.tag == "Walls"){
            Destroy(gameObject);
        }
    }

}
