﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtilleroManager : MonoBehaviour
{
    public GameObject artillero;
    private float randomPosition;
    public float timeLauncher;
    private float currentTime;

    void Update(){
        currentTime += Time.deltaTime;
        randomPosition = Random.Range(-8.21f, 8.21f);

        if(currentTime > timeLauncher){
            currentTime = 0;
            Instantiate( artillero, new Vector3(this.transform.position.x, 0f, randomPosition), Quaternion.identity, this.transform);
        }
    }
}
