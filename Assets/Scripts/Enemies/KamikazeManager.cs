﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KamikazeManager : MonoBehaviour
{
    public GameObject kamikaze;
    private float randomPosition;

    public float timeLauncher;
    private float currentTime;
    void Update(){
        currentTime += Time.deltaTime;
        randomPosition = Random.Range(-9.09f, 9.09f);

        if(currentTime >= timeLauncher){
            currentTime = 0;
            Instantiate( kamikaze, new Vector3(this.transform.position.x, 0f, randomPosition), Quaternion.identity, this.transform);
        }

    }
}
