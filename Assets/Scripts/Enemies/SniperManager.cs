﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperManager : MonoBehaviour
{
    public GameObject sniper;
    private float randomPosition;
    public float timeLauncher;
    private float currentTime;

    void Update(){
        currentTime += Time.deltaTime;
        randomPosition = Random.Range(-7.11f, 7.11f);

        if(currentTime >= timeLauncher){
            currentTime = 0;
            Instantiate( sniper, new Vector3(this.transform.position.x, 0f, randomPosition), Quaternion.identity, this.transform);
        }
    }
}
