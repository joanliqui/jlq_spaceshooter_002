﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArtillero : MonoBehaviour
{
    //Movement
    private float currentSpeed;
    public float speed = 5;
    private Rigidbody rb;
    //Shoots
    public int maxShoots = 25;
    public float cadencia = 0.1f; 
    public GameObject bullet;
    public float explDamage;

    //Vida
    public float maxHealth = 100f;
    private float currentHealth;

    //POWERUPS
    [SerializeField] GameObject[] powerUp;
    private float randomPU;
    private Vector3 puPosition;

    //SCORE
    private ScoreManager sm;
    public int score;



    void Awake(){
        rb = GetComponent<Rigidbody>();
        currentSpeed = speed;
        currentHealth = maxHealth;
        StartCoroutine(Atack());
        randomPU = Random.Range( 0f, 100f);
        sm = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    void FixedUpdate(){
        rb.velocity = new Vector3(-currentSpeed * Time.deltaTime, 0, 0f);
    }

    void Update(){

         puPosition = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z);
         if(currentHealth <= 0){
            if(randomPU <= 100f && randomPU >50f){
                Destroy(gameObject);
            }
            else if (randomPU <= 50f && randomPU > 38f){
                Instantiate (powerUp[0], puPosition, Quaternion.identity, null);
            }
            else if(randomPU <= 38f && randomPU > 20f){
                Instantiate (powerUp[1], puPosition, Quaternion.identity, null);
            }
            else if(randomPU <=20f && randomPU >= 0){
                Instantiate (powerUp[2], puPosition, Quaternion.identity, null);
            }
            print(randomPU);
            sm.AddScore(score);
            Destroy(gameObject);
        }
    }
    IEnumerator Atack(){
        while(true){
            yield return new WaitForSeconds(2f);
            currentSpeed = 0;
            for( int contador = 0; contador <= maxShoots; contador++){
                Instantiate (bullet, this.transform.position, Quaternion.identity, null);
                yield return new WaitForSeconds(cadencia);
            }
            currentSpeed = speed;
        }
    }

    void OnTriggerEnter(Collider other){
        if(other.tag == "PlayerBullet"){
            currentHealth -= other.GetComponent<BasicBullet>().bulletDamage; 
        }
        else if(other.tag =="Missile"){
            currentHealth -= other.GetComponent<Missile>().missileDamage;
        }
        else if(other.tag == "ShootgunBullet"){
            currentHealth -= other.GetComponent<ShootgunBullet>().shDamage;
        }
        else if(other.tag == "Player"){
            Destroy(gameObject);
        }
        else if(other.tag == "Walls"){
            Destroy(gameObject);
        }
    }
}
