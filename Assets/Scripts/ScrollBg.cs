﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBg : MonoBehaviour
{
    public float bgSpeed = 2.0f;
    private float textureOffset = 0.0f;
    private Material mat;
    private Vector2 scroll;
    

    void Awake (){
        mat = GetComponent<Renderer> ().material;
        scroll = new Vector2 (textureOffset, 0);
    }
    void Update (){
        textureOffset += (Time.deltaTime * bgSpeed);
        if(textureOffset >= 100){
            textureOffset -= 100;
        }

        scroll.x = textureOffset;
        mat.SetTextureOffset("_MainTex",scroll);
    }
}
