﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileAmmo : MonoBehaviour
{
    private Rigidbody rb;
    public float speed = 300f;

    void Awake(){
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate(){
        rb.velocity = new Vector3(-speed * Time.deltaTime, 0, 0);
    }


    public void OnTriggerEnter(Collider other){
        if(other.tag == "Player" || other.tag =="Walls"){
            Destroy(gameObject);
        }
    }
}
