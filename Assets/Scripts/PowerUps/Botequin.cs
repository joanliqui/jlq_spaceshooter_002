﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botequin : MonoBehaviour
{
    public float speed;
    public float curacion;

    void Update(){
        transform.Translate( -speed * Time.deltaTime, 0f, 0f);
    }

    void OnTriggerEnter(Collider other){
        if(other.tag =="Player"){
            Destroy(gameObject);
        }
        else if(other.tag == "Walls"){
            Destroy(gameObject);
        }
    }
}
